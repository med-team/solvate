Source: solvate
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Charles Plessy <charles-debian-nospam@plessy.org>
Section: non-free/science
XS-Autobuild: yes
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/solvate
Vcs-Git: https://salsa.debian.org/med-team/solvate.git
Homepage: https://www3.mpibpc.mpg.de/groups/grubmueller/start/index.html
Rules-Requires-Root: no

Package: solvate
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: arranges water molecules around protein structures
 For molecular dynamics simulations it is sometimes appropriate
 not to model in the vacuum but to have the proteins surrounded
 by their solvent. This program computes the location of water
 molecules such that the resulting PDB files become suitable
 for further analyses.

Package: solvate-doc
Architecture: all
Section: non-free/doc
Depends: ${misc:Depends}
Description: Documentation for solvate
 HTML documentation for solvate, the tool to prepare
 water cubes around proteins.
